using System;
using UnityEngine;

namespace UnityDots.Dialogue
{
    [Serializable]
    internal sealed class CharacterData
    {
        [SerializeField]
        private long _id;
        public long ID => _id;

        [SerializeField]
        private string _name;
        public string Name => _name;

        internal CharacterData(long id, string name)
        {
            _id = id;
            _name = name;
        }
    }
}
