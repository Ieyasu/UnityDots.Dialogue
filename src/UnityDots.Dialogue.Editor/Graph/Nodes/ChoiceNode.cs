using System.Collections.Generic;
using System.Linq;
using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Dialogue.Editors
{
    [Node(typeof(DialogueGraph), "Choice")]
    public sealed class ChoiceNode : NodeData, IMultiOutputNode
    {
        private const int _minSocketCount = 1;
        private const int _maxSocketCount = 32;

        [Input(0, Label = "In")]
        public DefaultSocket Input
        {
            get;
            set;
        }

        [SerializeField]
        private List<Dialogue> _choices = new List<Dialogue>();

        [SerializeField]
        private List<string> _conditions = new List<string>();

        public bool CanAddOutputSocket()
        {
            return OutputSockets.Count < _maxSocketCount;
        }

        public bool CanRemoveOutputSocket()
        {
            return OutputSockets.Count > _minSocketCount;
        }

        public void OnOutputSocketAdd()
        {
            CreateSocket(OutputSockets.Count);
            ResizeList();
        }

        public void OnOutputSocketRemove()
        {
            var id = OutputSockets.Last().ID;
            RemoveOutputSocket(id);
            ResizeList();
        }

        protected override void BuildSockets()
        {
            var socketCount = max(_conditions.Count, max(_choices.Count, 1));
            for (var i = 0; i < socketCount; ++i)
            {
                CreateSocket(i);
            }
            ResizeList();
        }

        private void CreateSocket(int id)
        {
            var socket = new DefaultSocket { ID = id, Name = "Out", Capacity = 1 };
            AddOutputSocket(socket);
        }

        private void ResizeList()
        {
            ResizeList(_choices);
            ResizeList(_conditions, "");
        }

        private void ResizeList<T>(List<T> list, T value = default)
        {
            while (list.Count > OutputSockets.Count)
            {
                list.RemoveAt(list.Count - 1);
            }

            while (list.Count < OutputSockets.Count)
            {
                list.Add(value);
            }
        }
    }
}
