using UnityDots.Editor;
using UnityEditor;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    public sealed class CharacterTableField : VisualElement
    {
        private readonly CharacterTable _table;
        private readonly ListField _listField;
        private readonly SerializedProperty _itemsProperty;

        public CharacterTableField(SerializedObject serializedObject)
        {
            _table = serializedObject.targetObject as CharacterTable;
            _itemsProperty = serializedObject.FindProperty("_data");
            _listField = new ListField(_itemsProperty, 24)
            {
                Text = "Characters",
                AddItem = AddItem,
                RemoveItem = RemoveItem
            };

            style.marginLeft = -20;
            style.marginRight = -4;
            style.marginBottom = -4;
            style.marginTop = -4;

            Add(_listField);
        }

        private void AddItem()
        {
            _itemsProperty.serializedObject.ApplyModifiedProperties();
            _table.InsertCharacter(0, "New Character");
            _itemsProperty.serializedObject.Update();
        }

        private void RemoveItem(int index)
        {
            _itemsProperty.serializedObject.ApplyModifiedProperties();
            _table.RemoveCharacterAtIndex(index);
            _itemsProperty.serializedObject.Update();
        }
    }
}
