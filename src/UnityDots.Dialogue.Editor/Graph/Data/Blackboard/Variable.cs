using System;
using UnityEngine;

namespace UnityDots.Dialogue
{
    [Serializable]
    public struct Variable
    {
        public static Variable Invalid => new Variable(0, null);

        [SerializeField]
        private long _id;
        public long ID
        {
            get
            {
                if (!IsValid)
                {
                    ThrowInvalid();
                }

                return _id;
            }
        }

        [SerializeField]
        private Blackboard _table;
        internal VariableData Data
        {
            get
            {
                if (!IsValid)
                {
                    ThrowInvalid();
                }

                return _table.GetVariableDataByID(_id);
            }
        }

        public string Name => Data.Name;
        public VariableType Type => Data.Type;
        public bool BoolValue => Data.BoolValue;
        public float FloatValue => Data.FloatValue;
        public int IntValue => Data.IntValue;
        public string StringValue => Data.StringValue;
        public bool IsValid => _table != null && _table.ContainsVariable(_id);

        internal Variable(long id, Blackboard blackboard)
        {
            _id = id;
            _table = blackboard;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Variable variable))
            {
                return false;
            }

            if (IsValid != variable.IsValid)
            {
                return false;
            }

            return !IsValid || (_id == variable._id && _table == variable._table);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_table, _id);
        }

        private void ThrowInvalid()
        {
            throw new System.InvalidOperationException("Variable pointer is invalid. Remember to check validity using Variable.IsValid()");
        }
    }
}
