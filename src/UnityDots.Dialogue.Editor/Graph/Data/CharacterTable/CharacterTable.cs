using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Dialogue
{
    [CreateAssetMenu(fileName = "Dialogue Characters.asset", menuName = "Dialogue/Character Table", order = 2)]
    public sealed class CharacterTable : ScriptableObject
    {
        private Dictionary<long, int> _idToIndexMap;
        private Dictionary<long, int> IDToIndexMap => GetIDToIndexMap();

        [SerializeField]
        private int _idCounter;

        [SerializeField]
        private List<CharacterData> _data;
        internal List<CharacterData> Data => _data;

        public int Count => Data.Count;

        public bool ContainsCharacter(long id)
        {
            return IDToIndexMap.ContainsKey(id);
        }

        public void RemoveCharacter(long id)
        {
            var index = IDToIndexMap[id];
            RemoveCharacterAtIndex(index);
        }

        public void RemoveCharacter(Character character)
        {
            RemoveCharacter(character.ID);
        }

        public void RemoveCharacterAtIndex(int index)
        {
            Data.RemoveAt(index);
        }

        public Character AddCharacter(string name)
        {
            var id = _idCounter++;
            var characterData = new CharacterData(id, name);
            IDToIndexMap.Add(id, _data.Count);
            Data.Add(characterData);
            return GetCharacterByID(id);
        }

        public Character InsertCharacter(int index, string name)
        {
            var id = _idCounter++;
            var characterData = new CharacterData(id, name);
            Data.Insert(index, characterData);
            RebuildIDToIndexMap();
            return GetCharacterByID(id);
        }

        public Character GetCharacterByID(long id)
        {
            return new Character(id, this);
        }

        public Character GetCharacterByIndex(int index)
        {
            var data = GetCharacterDataByIndex(index);
            return new Character(data.ID, this);
        }

        internal CharacterData GetCharacterDataByID(long id)
        {
            var index = IDToIndexMap[id];
            return GetCharacterDataByIndex(index);
        }

        internal CharacterData GetCharacterDataByIndex(int index)
        {
            return _data[index];
        }

        private Dictionary<long, int> GetIDToIndexMap()
        {
            if (_idToIndexMap == null)
            {
                RebuildIDToIndexMap();
            }

            return _idToIndexMap;
        }

        private void RebuildIDToIndexMap()
        {
            if (_idToIndexMap == null)
            {
                _idToIndexMap = new Dictionary<long, int>();
            }

            _idToIndexMap.Clear();
            for (var i = _data.Count - 1; i >= 0; --i)
            {
                var character = _data[i];
                if (!_idToIndexMap.ContainsKey(character.ID))
                {
                    _idToIndexMap.Add(character.ID, i);
                }
                else
                {
                    _data.RemoveAt(i);
                }
            }
        }
    }
}
