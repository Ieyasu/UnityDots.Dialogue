using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Dialogue.Editors
{
    [Node(typeof(DialogueGraph), "Dialogue")]
    public sealed class DialogueNode : NodeData
    {
        [Input(0, Label = "In")]
        public DefaultSocket Input
        {
            get;
            set;
        }

        [Output(0, Label = "Out")]
        public DefaultSocket Output
        {
            get;
            set;
        }

        [Control, SerializeField]
        private Character _speaker;
        public Character Speaker => _speaker;

        [Control, SerializeField]
        private Character _listener;
        public Character Listener => _listener;

        [Control, SerializeField]
        private Dialogue _dialogue;
    }
}
