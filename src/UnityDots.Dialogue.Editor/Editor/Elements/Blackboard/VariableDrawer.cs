using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    [CustomPropertyDrawer(typeof(Variable))]
    public sealed class VariableDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            return new VariableField()
            {
                BindingPath = property.propertyPath
            };
        }
    }
}
