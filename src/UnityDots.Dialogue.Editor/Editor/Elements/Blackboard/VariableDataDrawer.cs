using System;
using UnityDots.Editor;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace UnityDots.Dialogue.Editor
{
    [CustomPropertyDrawer(typeof(VariableData))]
    public sealed class VariableDataDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();
            var nameField = new TextField() { bindingPath = "_name" };
            var valueContainer = CreateValueGUI(property);

            container.LoadAndAddStyleSheet("Styles/VariableDataDrawer");
            container.AddToClassList("unitydots-variable-data");
            nameField.AddToClassList("unitydots-variable-data__name");

            container.Add(nameField);
            container.Add(valueContainer);

            return container;
        }

        private VisualElement CreateValueGUI(SerializedProperty property)
        {
            var valueContainer = new VisualElement();
            var typeProperty = property.FindPropertyRelative("_type");
            var typeField = new EnumField(VariableType.Boolean) { bindingPath = "_type" };
            var boolField = new Toggle() { bindingPath = "_boolValue" };
            var floatField = new FloatField() { bindingPath = "_floatValue" };
            var intField = new IntegerField() { bindingPath = "_intValue" };
            var stringField = new TextField() { bindingPath = "_stringValue", multiline = true };
            var objectField = new ObjectField() { bindingPath = "_objectValue", objectType = typeof(UnityEngine.Object) };

            valueContainer.Add(typeField);
            valueContainer.Add(boolField);
            valueContainer.Add(floatField);
            valueContainer.Add(intField);
            valueContainer.Add(stringField);
            valueContainer.Add(objectField);

            valueContainer.AddToClassList("unitydots-variable-data__var");
            typeField.AddToClassList("unitydots-variable-data__var-type");
            boolField.AddToClassList("unitydots-variable-data__var-value");
            floatField.AddToClassList("unitydots-variable-data__var-value");
            intField.AddToClassList("unitydots-variable-data__var-value");
            stringField.AddToClassList("unitydots-variable-data__var-value");
            objectField.AddToClassList("unitydots-variable-data__var-value");

            var initialValue = typeProperty.enumNames[typeProperty.enumValueIndex];
            if (Enum.TryParse(initialValue.ToString().Replace(" ", string.Empty), true, out VariableType initialType))
            {
                UpdateControl(initialType, VariableType.Boolean, boolField);
                UpdateControl(initialType, VariableType.Float, floatField);
                UpdateControl(initialType, VariableType.Integer, intField);
                UpdateControl(initialType, VariableType.String, stringField);
                UpdateControl(initialType, VariableType.Object, objectField);
            }

            typeField.RegisterValueChangedCallback(e =>
            {
                if (e.newValue == null)
                {
                    return;
                }

                if (Enum.TryParse(e.newValue.ToString().Replace(" ", string.Empty), true, out VariableType type))
                {
                    UpdateControl(type, VariableType.Boolean, boolField);
                    UpdateControl(type, VariableType.Float, floatField);
                    UpdateControl(type, VariableType.Integer, intField);
                    UpdateControl(type, VariableType.String, stringField);
                    UpdateControl(type, VariableType.Object, objectField);
                }
            });

            return valueContainer;
        }

        private void UpdateControl(VariableType type1, VariableType type2, VisualElement element)
        {
            element.style.display = type1 == type2 ? DisplayStyle.Flex : DisplayStyle.None;
        }
    }
}
