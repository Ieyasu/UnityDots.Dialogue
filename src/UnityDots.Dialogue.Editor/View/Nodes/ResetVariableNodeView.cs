using UnityDots.Graph.Editors;
using UnityDots.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    [CustomNodeView(typeof(ResetVariableNode), false)]
    public sealed class ResetVariableNodeView : NodeView
    {
        private DialogueGraph DialogueGraph => Node.Graph as DialogueGraph;
        private ResetVariableNode ResetVariableNode => Node as ResetVariableNode;

        protected override void BuildControls(VisualElement container, SerializedObject serializedObject)
        {
            var table = DialogueGraph.Blackboard;
            var variableProperty = serializedObject.FindProperty("_variable");

            var variableField = new VariableField(table)
            {
                BindingPath = variableProperty.propertyPath,
                VariableFieldLabel = "Variable"
            };
            variableField.RegisterCallback<ChangeEvent<Variable>>(OnVariableChanged);

            container.LoadAndAddStyleSheet("Styles/ResetVariableNodeView");
            container.Add(variableField);
            AddToClassList("unitydots-node-view--reset-variable");
        }

        private void OnVariableChanged(ChangeEvent<Variable> e)
        {
            if (e.newValue.ID != ResetVariableNode.Variable.ID)
            {
                OnNodeChanged();
            }
        }
    }
}
