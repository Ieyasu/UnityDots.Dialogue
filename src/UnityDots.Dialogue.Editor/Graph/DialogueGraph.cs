using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Tables;

namespace UnityDots.Dialogue
{
    [CreateAssetMenu(fileName = "Dialogue Graph.asset", menuName = "Dialogue/Graph", order = 0)]
    public sealed class DialogueGraph : GraphData
    {
        private StringTable _stringTable;

        public Character InvalidCharacter => new Character(0, null);

        [SerializeField]
        private long _keyIdCounter = 0;

        [SerializeField]
        private Blackboard _blackboard;
        public Blackboard Blackboard
        {
            get => _blackboard;
            set => _blackboard = value;
        }

        [SerializeField]
        private CharacterTable _characters;
        public CharacterTable CharacterTable
        {
            get => _characters;
            set => _characters = value;
        }

        [SerializeField]
        private LocalizedStringTable _dialogueTable = null;
        public LocalizedStringTable DialogueTable
        {
            get => _dialogueTable;
            set => _dialogueTable = value;
        }

        public LocalizedString CreateLocalizedString()
        {
            var entry = _stringTable.AddEntry(_keyIdCounter++, null);
            return new LocalizedString
            {
                TableReference = _dialogueTable.TableReference,
                TableEntryReference = entry.KeyId
            };
        }
    }
}
