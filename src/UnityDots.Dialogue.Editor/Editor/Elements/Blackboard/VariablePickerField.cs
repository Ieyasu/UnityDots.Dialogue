using System.Collections.Generic;
using UnityDots.Editor;
using UnityEngine.UIElements;
using UnityEngine;
using UnityEditor.UIElements;

namespace UnityDots.Dialogue.Editors
{
    internal sealed class VariablePickerField : PickerField<Variable>
    {
        private class VariableProvider : PickerProvider<Variable> { }

        private readonly ObjectField _tableField;
        private readonly LongField _idField;

        private string _bindingPath;
        public new string bindingPath
        {
            get => _bindingPath;
            set
            {
                _bindingPath = value;
                _tableField.bindingPath = $"{_bindingPath}._table";
                _idField.bindingPath = $"{_bindingPath}._id";
            }
        }

        public VariablePickerField(ObjectField tableField)
            : this(tableField, null)
        {
        }

        public VariablePickerField(ObjectField tableField, string label)
            : base(label)
        {
            AddToClassList(UssClassName);

            _tableField = tableField;
            _idField = new LongField();
            _idField.style.display = DisplayStyle.None;

            _tableField.RegisterValueChangedCallback((e) =>
            {
                RefreshValue();
                SetupProvider();
            });

            _idField.RegisterValueChangedCallback((e) =>
            {
                RefreshValue();
            });

            RegisterCallback<ChangeEvent<Variable>>((e) =>
            {
                SetLabel(e.newValue);
            });

            RegisterCallback<AttachToPanelEvent>((e) =>
            {
                RefreshValue();
                SetLabel(value);
                SetupProvider();
            });

            Add(_idField);
        }

        private void RefreshValue()
        {
            var id = _idField.value;
            if (_tableField.value is Blackboard table && table.ContainsVariable(id))
            {
                value = table.GetVariableByID(id);
            }
            else
            {
                value = Variable.Invalid;
            }
        }

        private void SetLabel(Variable variable)
        {
            if (variable.IsValid)
            {
                SetLabel(null, variable.Name);
            }
            else
            {
                SetLabel(null, "<None>");
            }
        }

        private void SetupProvider()
        {
            Provider = ScriptableObject.CreateInstance<VariableProvider>();

            var items = new List<Variable> { Variable.Invalid };
            var names = new List<string> { "<None>" };
            var table = _tableField.value as Blackboard;

            if (table != null)
            {
                for (var i = 0; i < table.Count; ++i)
                {
                    var variable = table.GetVariableByIndex(i);
                    items.Add(variable);
                    names.Add(variable.Name);
                }
            }

            Provider.Setup("Variables", names, items, null, (selected) =>
            {
                if (!selected.IsValid)
                {
                    _idField.value = -1;
                }
                else if (_idField.value != selected.ID)
                {
                    _idField.value = selected.ID;
                }
            });
        }
    }
}
