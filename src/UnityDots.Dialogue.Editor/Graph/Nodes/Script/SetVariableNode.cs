using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Dialogue.Editors
{
    [Node(typeof(DialogueGraph), "Script/Set Variable")]
    public sealed class SetVariableNode : NodeData
    {
        [Input(0, Label = "In")]
        public DefaultSocket Input
        {
            get;
            set;
        }

        [Output(0, Label = "Out")]
        public DefaultSocket Output
        {
            get;
            set;
        }

        [Control, SerializeField]
        private Variable _variable;
        public Variable Variable
        {
            get => _variable;
            set => _variable = value;
        }
    }
}
