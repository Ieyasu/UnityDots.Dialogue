using System.Collections.Generic;
using UnityDots.Editor;
using UnityEngine.UIElements;
using UnityEngine;
using UnityEditor.UIElements;

namespace UnityDots.Dialogue.Editors
{
    internal sealed class CharacterPickerField : PickerField<Character>
    {
        private class CharacterProvider : PickerProvider<Character> { }

        private readonly ObjectField _tableField;
        private readonly LongField _idField;

        private string _bindingPath;
        public new string bindingPath
        {
            get => _bindingPath;
            set
            {
                _bindingPath = value;
                _tableField.bindingPath = $"{_bindingPath}._table";
                _idField.bindingPath = $"{_bindingPath}._id";
            }
        }

        public CharacterPickerField(ObjectField tableField)
            : this(tableField, null)
        {
        }

        public CharacterPickerField(ObjectField tableField, string label)
            : base(label)
        {
            AddToClassList(UssClassName);

            _tableField = tableField;
            _idField = new LongField();
            _idField.style.display = DisplayStyle.None;

            _tableField.RegisterValueChangedCallback((e) =>
            {
                RefreshValue();
                SetupProvider();
            });

            _idField.RegisterValueChangedCallback((e) =>
            {
                RefreshValue();
            });

            RegisterCallback<ChangeEvent<Character>>((e) =>
            {
                SetLabel(e.newValue);
            });

            RegisterCallback<AttachToPanelEvent>((e) =>
            {
                RefreshValue();
                SetLabel(value);
                SetupProvider();
            });

            Add(_idField);
        }

        private void RefreshValue()
        {
            var id = _idField.value;
            if (_tableField.value is CharacterTable table && table.ContainsCharacter(id))
            {
                value = table.GetCharacterByID(id);
            }
            else
            {
                value = Character.Invalid;
            }
        }

        private void SetLabel(Character character)
        {
            if (character.IsValid)
            {
                SetLabel(null, character.Name);
            }
            else
            {
                SetLabel(null, "<None>");
            }
        }

        private void SetupProvider()
        {
            Provider = ScriptableObject.CreateInstance<CharacterProvider>();

            var items = new List<Character> { Character.Invalid };
            var names = new List<string> { "<None>" };
            var table = _tableField.value as CharacterTable;

            if (table != null)
            {
                for (var i = 0; i < table.Count; ++i)
                {
                    var character = table.GetCharacterByIndex(i);
                    items.Add(character);
                    names.Add(character.Name);
                }
            }

            Provider.Setup("Characters", names, items, null, (selected) =>
            {
                if (!selected.IsValid)
                {
                    _idField.value = -1;
                }
                else if (_idField.value != selected.ID)
                {
                    _idField.value = selected.ID;
                }
            });
        }
    }
}
