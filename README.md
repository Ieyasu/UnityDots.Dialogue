# UnityDots Dialogue

UnityDots Dialogue is a graph based dialogue editor.

## Features

* Global/local variables
* Localization support (WIP)
* Support for characters
* Support for multiple dialogue graphs

## Getting started

To include the package in your Unity project, add the following git url using the Package Manager UI:

```
https://gitlab.com/Ieyasu/UnityDots.Mathematics.git?path=/src"
https://gitlab.com/Ieyasu/UnityDots.Common.git?path=/src"
https://gitlab.com/Ieyasu/UnityDots.Graph.git?path=/src"
https://gitlab.com/Ieyasu/UnityDots.Dialogue.git?path=/src"
```

TODO: Rest of the guide
