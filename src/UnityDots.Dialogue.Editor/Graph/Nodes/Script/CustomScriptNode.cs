using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Dialogue.Editors
{
    [Node(typeof(DialogueGraph), "Script/Custom Script")]
    public sealed class CustomScriptNode : NodeData
    {
        [Input(0, Label = "In")]
        public DefaultSocket Input
        {
            get;
            set;
        }

        [Output(0, Label = "Out")]
        public DefaultSocket Output
        {
            get;
            set;
        }

        [Control, SerializeField]
        private string _script = null;
    }
}
