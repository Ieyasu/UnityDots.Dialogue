using UnityDots.Editor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEngine;

namespace UnityDots.Dialogue.Editors
{
    public sealed class VariableField : VisualElement
    {
        private readonly ObjectField _tableField;
        private readonly VariablePickerField _variablePickerField;

        public string TableFieldLabel
        {
            get => _tableField.label;
            set => _tableField.label = value;
        }

        public string VariableFieldLabel
        {
            get => _variablePickerField.label;
            set => _variablePickerField.label = value;
        }

        private bool _showTableSelector;
        public bool ShowTableSelector
        {
            get => _showTableSelector;
            set
            {
                _showTableSelector = value;
                _tableField.SetVisible(value);
            }
        }

        private string _bindingPath;
        public string BindingPath
        {
            get => _bindingPath;
            set
            {
                if (_bindingPath == value)
                {
                    return;
                }

                _bindingPath = value;
                _variablePickerField.bindingPath = _bindingPath;
                if (_tableField != null)
                {
                    _tableField.bindingPath = $"{_bindingPath}._table";
                }
            }
        }

        public VariableField(bool showTableSelector = true)
        {
            _tableField = new ObjectField("Table")
            {
                objectType = typeof(Blackboard)
            };
            _variablePickerField = new VariablePickerField(_tableField, "Variable");

            Add(_tableField);
            Add(_variablePickerField);

            ShowTableSelector = showTableSelector;
        }

        public VariableField(Blackboard table, bool showTableSelector = false)
            : this(showTableSelector)
        {
            RegisterCallback<AttachToPanelEvent>((e) =>
            {
                _tableField.value = table;
            });
        }
    }
}
