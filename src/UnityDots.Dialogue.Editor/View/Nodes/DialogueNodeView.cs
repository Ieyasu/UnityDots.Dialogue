using UnityDots.Graph.Editors;
using UnityDots.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    [CustomNodeView(typeof(DialogueNode), false)]
    public sealed class DialogueNodeView : NodeView
    {
        private DialogueNode DialogueNode => Node as DialogueNode;
        private DialogueGraph DialogueGraph => Node.Graph as DialogueGraph;

        protected override void BuildControls(VisualElement container, SerializedObject serializedObject)
        {
            var table = DialogueGraph.CharacterTable;
            var speakerProperty = serializedObject.FindProperty("_speaker");
            var listenerProperty = serializedObject.FindProperty("_listener");

            var speakerField = new CharacterField(table)
            {
                BindingPath = speakerProperty.propertyPath,
                CharacterFieldLabel = "Speaker"
            };
            var listenerField = new CharacterField(table)
            {
                BindingPath = listenerProperty.propertyPath,
                CharacterFieldLabel = "Listener"
            };
            var dialogueField = new TextField(null)
            {
                multiline = true
            };

            speakerField.RegisterCallback<ChangeEvent<Character>>(OnSpeakerChanged);
            listenerField.RegisterCallback<ChangeEvent<Character>>(OnListenerChanged);

            container.LoadAndAddStyleSheet("Styles/DialogueNodeView");
            container.Add(speakerField);
            container.Add(listenerField);
            container.Add(dialogueField);
            AddToClassList("unitydots-node-view--dialogue");
        }

        private void OnSpeakerChanged(ChangeEvent<Character> e)
        {
            if (e.newValue.ID != DialogueNode.Speaker.ID)
            {
                OnNodeChanged();
            }
        }

        private void OnListenerChanged(ChangeEvent<Character> e)
        {
            if (e.newValue.ID != DialogueNode.Listener.ID)
            {
                OnNodeChanged();
            }
        }
    }
}
