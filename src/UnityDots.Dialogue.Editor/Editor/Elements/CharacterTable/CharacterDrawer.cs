using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    [CustomPropertyDrawer(typeof(Character))]
    public sealed class CharacterDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            return new CharacterField()
            {
                BindingPath = property.propertyPath
            };
        }
    }
}
