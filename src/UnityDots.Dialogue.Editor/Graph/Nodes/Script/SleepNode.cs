using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Dialogue.Editors
{
    [Node(typeof(DialogueGraph), "Script/Sleep")]
    public sealed class SleepNode : NodeData
    {
        [Input(0, Label = "In")]
        public DefaultSocket Input
        {
            get;
            set;
        }

        [Output(0, Label = "Out")]
        public DefaultSocket Output
        {
            get;
            set;
        }

        [Control, SerializeField]
        private float _time = 1;
    }
}
