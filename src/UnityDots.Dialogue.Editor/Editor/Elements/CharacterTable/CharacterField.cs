using UnityDots.Editor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEngine;

namespace UnityDots.Dialogue.Editors
{
    public sealed class CharacterField : VisualElement
    {
        private readonly ObjectField _tableField;
        private readonly CharacterPickerField _characterPickerField;

        public string TableFieldLabel
        {
            get => _tableField.label;
            set => _tableField.label = value;
        }

        public string CharacterFieldLabel
        {
            get => _characterPickerField.label;
            set => _characterPickerField.label = value;
        }

        private bool _showTableSelector;
        public bool ShowTableSelector
        {
            get => _showTableSelector;
            set
            {
                _showTableSelector = value;
                _tableField.SetVisible(value);
            }
        }

        private string _bindingPath;
        public string BindingPath
        {
            get => _bindingPath;
            set
            {
                if (_bindingPath == value)
                {
                    return;
                }

                _bindingPath = value;
                _characterPickerField.bindingPath = _bindingPath;
                if (_tableField != null)
                {
                    _tableField.bindingPath = $"{_bindingPath}._table";
                }
            }
        }

        public CharacterField(bool showTableSelector = true)
        {
            _tableField = new ObjectField("Table")
            {
                objectType = typeof(CharacterTable)
            };
            _characterPickerField = new CharacterPickerField(_tableField, "Character");

            Add(_tableField);
            Add(_characterPickerField);

            ShowTableSelector = showTableSelector;
        }

        public CharacterField(CharacterTable table, bool showTableSelector = false)
            : this(showTableSelector)
        {
            RegisterCallback<AttachToPanelEvent>((e) =>
            {
                _tableField.value = table;
            });
        }
    }
}
