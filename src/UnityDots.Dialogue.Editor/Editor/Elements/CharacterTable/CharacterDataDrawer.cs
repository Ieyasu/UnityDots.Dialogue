using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace UnityDots.Dialogue.Editor
{
    [CustomPropertyDrawer(typeof(CharacterData))]
    public sealed class CharacterDataDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var nameField = new TextField() { bindingPath = "_name", multiline = false };

            var container = new VisualElement();
            container.Add(nameField);
            return container;
        }
    }
}
