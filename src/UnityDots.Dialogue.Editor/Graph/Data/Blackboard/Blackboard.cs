using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Dialogue
{
    [CreateAssetMenu(fileName = "Dialogue Blackboard.asset", menuName = "Dialogue/Blackboard", order = 1)]
    public sealed class Blackboard : ScriptableObject
    {
        private Dictionary<long, int> _idToIndexMap;
        private Dictionary<long, int> IDToIndexMap => GetIDToIndexMap();

        [SerializeField]
        private int _idCounter;

        [SerializeField]
        private List<VariableData> _data;
        internal List<VariableData> Data => _data;

        public int Count => Data.Count;

        public bool ContainsVariable(long id)
        {
            return IDToIndexMap.ContainsKey(id);
        }

        public void RemoveVariable(long id)
        {
            var index = IDToIndexMap[id];
            RemoveVariableAtIndex(index);
        }

        public void RemoveVariable(Variable variable)
        {
            RemoveVariable(variable.ID);
        }

        public void RemoveVariableAtIndex(int index)
        {
            Data.RemoveAt(index);
        }

        public Variable AddBoolean(string name, bool value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return AddVariable(variableData);
        }

        public Variable AddFloat(string name, float value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return AddVariable(variableData);
        }

        public Variable AddInteger(string name, int value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return AddVariable(variableData);
        }

        public Variable AddString(string name, string value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return AddVariable(variableData);
        }

        public Variable AddObject(string name, Object value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return AddVariable(variableData);
        }

        public Variable InsertBoolean(int index, string name, bool value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return InsertVariable(index, variableData);
        }

        public Variable InsertFloat(int index, string name, float value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return InsertVariable(index, variableData);
        }

        public Variable InsertInteger(int index, string name, int value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return InsertVariable(index, variableData);
        }

        public Variable InsertString(int index, string name, string value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return InsertVariable(index, variableData);
        }

        public Variable InsertObject(int index, string name, Object value = default)
        {
            var variableData = new VariableData(_idCounter++, name, value);
            return InsertVariable(index, variableData);
        }

        public Variable GetVariableByID(long id)
        {
            return new Variable(id, this);
        }

        public Variable GetVariableByIndex(int index)
        {
            var data = GetVariableDataByIndex(index);
            return new Variable(data.ID, this);
        }

        internal VariableData GetVariableDataByID(long id)
        {
            var index = IDToIndexMap[id];
            return GetVariableDataByIndex(index);
        }

        internal VariableData GetVariableDataByIndex(int index)
        {
            return _data[index];
        }

        private Variable AddVariable(VariableData variableData)
        {
            IDToIndexMap.Add(variableData.ID, _data.Count);
            Data.Add(variableData);
            return GetVariableByID(variableData.ID);
        }

        private Variable InsertVariable(int index, VariableData variableData)
        {
            Data.Insert(index, variableData);
            RebuildIDToIndexMap();
            return GetVariableByID(variableData.ID);
        }

        private Dictionary<long, int> GetIDToIndexMap()
        {
            if (_idToIndexMap == null)
            {
                RebuildIDToIndexMap();
            }

            return _idToIndexMap;
        }

        private void RebuildIDToIndexMap()
        {
            if (_idToIndexMap == null)
            {
                _idToIndexMap = new Dictionary<long, int>();
            }

            _idToIndexMap.Clear();
            for (var i = _data.Count - 1; i >= 0; --i)
            {
                var variable = _data[i];
                if (!_idToIndexMap.ContainsKey(variable.ID))
                {
                    _idToIndexMap.Add(variable.ID, i);
                }
                else
                {
                    _data.RemoveAt(i);
                }
            }
        }
    }
}
