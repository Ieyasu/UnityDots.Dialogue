using UnityDots.Editor;
using UnityEditor;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    [CustomEditor(typeof(CharacterTable))]
    public sealed class CharacterTableEditor : UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {
            var field = new CharacterTableField(serializedObject);
            return field;
        }
    }
}
