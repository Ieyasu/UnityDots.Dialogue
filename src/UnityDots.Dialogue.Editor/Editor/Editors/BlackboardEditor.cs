using UnityEditor;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    [CustomEditor(typeof(Blackboard))]
    public sealed class BlackboardEditor : UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {
            var field = new BlackboardField(serializedObject);
            return field;
        }
    }
}
