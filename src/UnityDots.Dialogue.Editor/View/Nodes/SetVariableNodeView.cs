using UnityDots.Graph.Editors;
using UnityDots.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    [CustomNodeView(typeof(SetVariableNode), false)]
    public sealed class SetVariableNodeView : NodeView
    {
        private DialogueGraph DialogueGraph => Node.Graph as DialogueGraph;
        private SetVariableNode SetVariableNode => Node as SetVariableNode;

        protected override void BuildControls(VisualElement container, SerializedObject serializedObject)
        {
            var table = DialogueGraph.Blackboard;
            var variableProperty = serializedObject.FindProperty("_variable");

            var variableField = new VariableField(table)
            {
                BindingPath = variableProperty.propertyPath,
                VariableFieldLabel = "Variable"
            };
            variableField.RegisterCallback<ChangeEvent<Variable>>(OnVariableChanged);

            container.LoadAndAddStyleSheet("Styles/SetVariableNodeView");
            container.Add(variableField);
            AddToClassList("unitydots-node-view--set-variable");
        }

        private void OnVariableChanged(ChangeEvent<Variable> e)
        {
            if (e.newValue.ID != SetVariableNode.Variable.ID)
            {
                OnNodeChanged();
            }
        }
    }
}
