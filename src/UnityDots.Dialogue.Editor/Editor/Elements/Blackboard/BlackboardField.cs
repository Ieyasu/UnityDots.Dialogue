using UnityDots.Editor;
using UnityEditor;
using UnityEngine.UIElements;

namespace UnityDots.Dialogue.Editors
{
    public sealed class BlackboardField : VisualElement
    {
        private readonly Blackboard _table;
        private readonly ListField _listField;
        private readonly SerializedProperty _itemsProperty;

        public BlackboardField(SerializedObject serializedObject)
        {
            _table = serializedObject.targetObject as Blackboard;
            _itemsProperty = serializedObject.FindProperty("_data");
            _listField = new ListField(_itemsProperty, 48)
            {
                Text = "Variables",
                AddItem = AddItem,
                RemoveItem = RemoveItem
            };

            style.marginLeft = -20;
            style.marginRight = -4;
            style.marginBottom = -4;
            style.marginTop = -4;

            Add(_listField);
        }

        private void AddItem()
        {
            _itemsProperty.serializedObject.ApplyModifiedProperties();
            _table.InsertBoolean(0, "NEW_VARIABLE");
            _itemsProperty.serializedObject.Update();
        }

        private void RemoveItem(int index)
        {
            _itemsProperty.serializedObject.ApplyModifiedProperties();
            _table.RemoveVariableAtIndex(index);
            _itemsProperty.serializedObject.Update();
        }
    }
}
