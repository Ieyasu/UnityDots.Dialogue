using System;
using UnityEngine;

namespace UnityDots.Dialogue
{
    [Serializable]
    public struct Character
    {
        public static Character Invalid => new Character(0, null);

        [SerializeField]
        private long _id;
        public long ID
        {
            get
            {
                if (!IsValid)
                {
                    // ThrowInvalid();
                }

                return _id;
            }
        }

        [SerializeField]
        private CharacterTable _table;
        internal CharacterData Data
        {
            get
            {
                if (!IsValid)
                {
                    ThrowInvalid();
                }

                return _table.GetCharacterDataByID(ID);
            }
        }

        public string Name => Data.Name;
        public bool IsValid => _table != null && _table.ContainsCharacter(_id);

        internal Character(long id, CharacterTable table)
        {
            _id = id;
            _table = table;
        }

        public bool Equals(Character character)
        {
            if (IsValid != character.IsValid)
            {
                return false;
            }

            return !IsValid || (_id == character._id && _table == character._table);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Character character))
            {
                return false;
            }

            return Equals(character);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_table, _id);
        }

        public override string ToString()
        {
            if (!IsValid)
            {
                return $"Character(INVALID)";
            }

            return $"Character(ID={ID}, Name={Name})";
        }
        private void ThrowInvalid()
        {
            throw new System.InvalidOperationException("Character pointer is invalid. Remember to check validity using Character.IsValid()");
        }
    }
}
