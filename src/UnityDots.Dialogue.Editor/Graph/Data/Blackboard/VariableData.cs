using System;
using UnityEngine;

namespace UnityDots.Dialogue
{
    [Serializable]
    internal sealed class VariableData
    {
        [SerializeField]
        private long _id;
        public long ID
        {
            get => _id;
            set => _id = value;
        }

        [SerializeField]
        private string _name;
        public string Name
        {
            get => _name;
            set => _name = value;
        }

        [SerializeField]
        private VariableType _type;
        public VariableType Type
        {
            get => _type;
            set => _type = value;
        }

        [SerializeField]
        private bool _boolValue;
        public bool BoolValue
        {
            get => _boolValue;
            set => _boolValue = value;
        }

        [SerializeField]
        private float _floatValue;
        public float FloatValue
        {
            get => _floatValue;
            set => _floatValue = value;
        }

        [SerializeField]
        private int _intValue;
        public int IntValue
        {
            get => _intValue;
            set => _intValue = value;
        }

        [SerializeField]
        private string _stringValue;
        public string StringValue
        {
            get => _stringValue;
            set => _stringValue = value;
        }

        [SerializeField]
        private UnityEngine.Object _objectValue;
        public UnityEngine.Object ObjectValue
        {
            get => _objectValue;
            set => _objectValue = value;
        }

        public VariableData(long id, string name, bool value)
        {
            _id = id;
            _name = name;
            _type = VariableType.Boolean;
            _boolValue = value;
        }

        public VariableData(long id, string name, float value)
        {
            _id = id;
            _name = name;
            _type = VariableType.Float;
            _floatValue = value;
        }

        public VariableData(long id, string name, int value)
        {
            _id = id;
            _name = name;
            _type = VariableType.Integer;
            _intValue = value;
        }

        public VariableData(long id, string name, string value)
        {
            _id = id;
            _name = name;
            _type = VariableType.String;
            _stringValue = value;
        }

        public VariableData(long id, string name, UnityEngine.Object value)
        {
            _id = id;
            _name = name;
            _type = VariableType.Object;
            _objectValue = value;
        }
    }
}
