using UnityEngine;
using UnityEngine.Localization;

namespace UnityDots.Dialogue
{
    [System.Serializable]
    public struct Dialogue
    {
        [SerializeField]
        private LocalizedString _dialogue;
    }
}
