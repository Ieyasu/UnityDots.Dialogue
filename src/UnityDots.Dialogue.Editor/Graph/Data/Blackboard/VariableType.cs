namespace UnityDots.Dialogue
{
    public enum VariableType
    {
        Boolean = 0,
        Float = 1,
        Integer = 2,
        String = 3,
        Object = 4
    }
}
